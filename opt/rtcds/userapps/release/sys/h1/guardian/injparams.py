from collections import OrderedDict

# Log directory
log_dir = "/ligo/www/www/exports/pem/WeeklyMagneticInjection/logs"

# Automated start time
day = 1 # Tuesday
starttime = '07:20:00' # local timezone

# Some injection parameters
duration = 30
ramptime = 5
timebetween = 5

# GDS path numbers for channel names
gds_paths = {
    'CS': [0, 2, 6], # vertex, CER, sqz-bay
    'EX': [1, 2],    # VEA, e-bay
    'EY': [1, 2]     # VEA, e-bay
}

# Excitation channel format EXCLUDING H1/L1 and _EXC
chan_form = 'PEM-{}_GDS_{}'

nln_statenum = 600

pwrunits = ['PULIZZI_ACPWRCTRL_VERTEX0', 'PULIZZI_ACPWRCTRL_SQZ0', 'PULIZZI_ACPWRCTRL_CER0', 'PULIZZI_ACPWRCTRL_EX0', 
            'TRIPPLITE_ACPWRCTRL_EX1', 'PULIZZI_ACPWRCTRL_EY0', 'TRIPPLITE_ACPWRCTRL_EY1']

pwrunits_dict = {
    'CS_0': 'PULIZZI_ACPWRCTRL_VERTEX0',
    'CS_2': 'PULIZZI_ACPWRCTRL_CER0',
    'CS_6': 'PULIZZI_ACPWRCTRL_SQZ0',
    'EX_1': 'TRIPPLITE_ACPWRCTRL_EX1',
    'EX_2': 'PULIZZI_ACPWRCTRL_EX0',
    'EY_1': 'TRIPPLITE_ACPWRCTRL_EY1',
    'EY_2': 'PULIZZI_ACPWRCTRL_EY0'
    }

#########################################################
'''
INJECTIONS RUN IN THE ORDER LISTED IN THIS DICTIONARY
'''
params = OrderedDict({
    'CS_VERTEX_LOW': {'station':   'CS',
                      'location':  0, # number in channel name associated with the appropriate coil
                      'freq_low':  10,
                      'freq_high': 100,
                      'gain':      50000,
                      'index':     10, # state index; helpful to assign these for each injection
                      'pwrunit':   'PULIZZI_ACPWRCTRL_VERTEX0'
                      },
    
    'CS_VERTEX_MID': {'station':   'CS',
                       'location':  0,
                       'freq_low':  100,
                       'freq_high': 1000,
                       'gain':      3000,
                       'index':     20,
                       'pwrunit':   'PULIZZI_ACPWRCTRL_VERTEX0'
                       },
     
    'CS_VERTEX_HIGH': {'station':   'CS',
                       'location':  0,
                       'freq_low':  1000,
                       'freq_high': 4000,
                       'gain':      7000,
                       'index':     30,
                       'pwrunit':   'PULIZZI_ACPWRCTRL_VERTEX0'
                       },
    
    'EX_VEA_LOW': {'station':   'EX',
                   'location':  1,
                   'freq_low':  10,
                   'freq_high': 100,
                   'gain':      14000,
                   'index':     40,
                   'pwrunit':   'TRIPPLITE_ACPWRCTRL_EX1'
                   },

    'EX_VEA_MID': {'station':   'EX',
                   'location':  1,
                   'freq_low':  100,
                   'freq_high': 1000,
                   'gain':      6000,
                   'index':     50,
                   'pwrunit':   'TRIPPLITE_ACPWRCTRL_EX1'
                   },
    
    'EX_VEA_HIGH': {'station':   'EX',
                    'location':  1,
                    'freq_low':  1000,
                    'freq_high': 4000,
                    'gain':      4000,
                    'index':     60,
                    'pwrunit':   'TRIPPLITE_ACPWRCTRL_EX1'
                    },

    'EY_VEA_LOW': {'station':   'EY',
                   'location':  1,
                   'freq_low':  10,
                   'freq_high': 100,
                   'gain':      15000,
                   'index':     70,
                   'pwrunit':   'TRIPPLITE_ACPWRCTRL_EY1'
                   },
    
    'EY_VEA_MID': {'station':   'EY',
                    'location':  1,
                    'freq_low':  100,
                    'freq_high': 1000,
                    'gain':      20000,
                    'index':     80,
                    'pwrunit':   'TRIPPLITE_ACPWRCTRL_EY1'
                    },

    'EY_VEA_HIGH': {'station':   'EY',
                    'location':  1,
                    'freq_low':  1000,
                    'freq_high': 4000,
                    'gain':      5000,
                    'index':     90,
                    'pwrunit':   'TRIPPLITE_ACPWRCTRL_EY1'
                    },     
    
    'CS_CER_LOW': {'station':   'CS',
                   'location':  2,
                   'freq_low':  10,
                   'freq_high': 100,
                   'gain':      1600,
                   'index':     100,
                   'pwrunit':   'PULIZZI_ACPWRCTRL_CER0'
                   },

    'CS_CER_MID': {'station':   'CS',
                   'location':  2,
                   'freq_low':  100,
                   'freq_high': 1000,
                   'gain':      2000,
                   'index':     110,
                   'pwrunit':   'PULIZZI_ACPWRCTRL_CER0'
                   },
    
    'CS_CER_HIGH': {'station':   'CS',
                    'location':  2,
                    'freq_low':  1000,
                    'freq_high': 4000,
                    'gain':      800,
                    'index':     120,
                    'pwrunit':   'PULIZZI_ACPWRCTRL_CER0'
                    },
    
    'EX_EBAY_LOW': {'station':   'EX',
                    'location':  2,
                    'freq_low':  10,
                    'freq_high': 100,
                    'gain':      650,
                    'index':     130,
                    'pwrunit':   'PULIZZI_ACPWRCTRL_EX0'
                    },
                    
    'EX_EBAY_MID': {'station':   'EX',
                    'location':  2,
                    'freq_low':  100,
                    'freq_high': 1000,
                    'gain':      800,
                    'index':     140,
                    'pwrunit':   'PULIZZI_ACPWRCTRL_EX0'
                    },
    
    'EX_EBAY_HIGH': {'station':   'EX',
                     'location':  2,
                     'freq_low':  1000,
                     'freq_high': 4000,
                     'gain':      1000,
                     'index':     150,
                     'pwrunit':   'PULIZZI_ACPWRCTRL_EX0'
                     },

    'EY_EBAY_LOW': {'station':   'EY',
                    'location':  2,
                    'freq_low':  10,
                    'freq_high': 100,
                    'gain':      900,
                    'index':     160,
                    'pwrunit':   'PULIZZI_ACPWRCTRL_EY0'
                    },
    
    'EY_EBAY_MID': {'station':   'EY',
                    'location':  2,
                    'freq_low':  100,
                    'freq_high': 1000,
                    'gain':      1200,
                    'index':     170,
                    'pwrunit':   'PULIZZI_ACPWRCTRL_EY0'
                    },

    'EY_EBAY_HIGH': {'station':   'EY',
                     'location':  2,
                     'freq_low':  1000,
                     'freq_high': 4000,
                     'gain':      800,
                     'index':     180,
                     'pwrunit':   'PULIZZI_ACPWRCTRL_EY0'
                     },
    
    'CS_SQZBAY_LOW': {'station':   'CS',
                      'location':  6,
                      'freq_low':  10,
                      'freq_high': 100,
                      'gain':      11100,
                      'index':     190,
                      'pwrunit':   'PULIZZI_ACPWRCTRL_SQZ0'
                      },
                    
    'CS_SQZBAY_MID': {'station':   'CS',
                      'location':  6,
                      'freq_low':  100,
                      'freq_high': 1000,
                      'gain':      3000,
                      'index':     200,
                      'pwrunit':   'PULIZZI_ACPWRCTRL_SQZ0'
                      },

    'CS_SQZBAY_HIGH': {'station':   'CS',
                       'location':  6,
                       'freq_low':  1000,
                       'freq_high': 4000,
                       'gain':      1000,
                       'index':     210,
                       'pwrunit':   'PULIZZI_ACPWRCTRL_SQZ0'
                       }
})
    

