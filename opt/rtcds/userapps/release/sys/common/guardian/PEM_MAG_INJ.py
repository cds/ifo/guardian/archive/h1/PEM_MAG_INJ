from guardian import GuardState, GuardStateDecorator
import time
import os
import sys
import awg
import math
import injparams
from gpstime import gpsnow
from datetime import date

request = 'INJECTIONS_COMPLETE'
nominal = 'WAITING'

# Log file that will contain injection parameters
LOG_DIR = injparams.log_dir
LOG_FILE = {'file': None}

# Global injection parameters
RAMP_TIME = injparams.ramptime
DURATION = injparams.duration
TIME_BETWEEN_INJECTIONS = injparams.timebetween

# Import things from site const 
gds_paths = injparams.gds_paths
CHAN = injparams.chan_form # Site channel format
params = injparams.params # Big params dictionary
site = os.getenv('IFO')

# Dictionary to store injections as they run
inj = {}

manual = {'is_manual': False}

standdown_sources = ['snews', 'fermi', 'swift']

# Functions
def initialize_exc(channel):
    # Create an "awg.Excitation" object with the desired injection parameters.

    # Uniform noise Excitation
    a = awg.UniformNoise(channel, ampl=1.0, freq1=.1, freq2=4000)
    return a

# Decorators
class assert_IFO_inlock(GuardStateDecorator):
    def pre_exec(self):
        lock_state = ezca['GRD-ISC_LOCK_STATE_N'] 
        if lock_state != injparams.nln_statenum and not manual['is_manual']: 
            notify('No longer in low noise, stopping injections')
            return 'DOWN'

def check_pwrunit(station, chan_num):
    class check_pwrunit_decorator(GuardStateDecorator):
        def pre_exec(self):
            if site == 'H1':
                if ezca[f'CDS-{injparams.pwrunits_dict[f"{station}_{chan_num}"]}_UNIT_ERR_MESSAGE']:
                    notify('Power unit has error, skipping this injection')
                    return True
    
    return check_pwrunit_decorator

edges = [
    ('DOWN','WAITING'),
    ('WAITING','PREP_MANUAL_INJECTIONS'),
    ('WAITING', 'TAKE_BACKGROUND'),
    ('PREP_MANUAL_INJECTIONS', 'TAKE_BACKGROUND')]

##################### State generator ###########################
def gen_MAG_INJ(station, chan_num, freq_low, freq_high, gain, ind):
    class INJECTION(GuardState):
        index = ind
        request = False
        
        @assert_IFO_inlock
        @check_pwrunit(station, chan_num)
        def main(self):
            if LOG_FILE['file'] is None:
                LOG_FILE['file'] = os.path.join(LOG_DIR, '{:.0f}'.format(gpsnow()) + '.txt')
            self.channel = site + ':' + CHAN.format(station, chan_num) + '_EXC'
            self.freq_low = freq_low
            self.freq_high = freq_high
            self.gain = gain
            # Get bandpass filter
            if self.freq_low == 10 and self.freq_high == 100:
                ezca.get_LIGOFilter(CHAN.format(station, chan_num)).only_on('INPUT', 'OUTPUT', 'DECIMATION', 'FM1')
            if self.freq_low == 100 and self.freq_high == 1000:
                ezca.get_LIGOFilter(CHAN.format(station, chan_num)).only_on('INPUT', 'OUTPUT', 'DECIMATION', 'FM2')
            if self.freq_low == 1000 and self.freq_high == 4000:
                ezca.get_LIGOFilter(CHAN.format(station, chan_num)).only_on('INPUT', 'OUTPUT', 'DECIMATION', 'FM3')
            ezca.get_LIGOFilter(CHAN.format(station, chan_num)).ramp_gain(1, ramp_time=0)
            # Create an 'awg.Excitation' object for this channel
            inj['{} {}-{}Hz'.format(station, freq_low, freq_high)] = initialize_exc(channel=self.channel)
            inj['{} {}-{}Hz'.format(station, freq_low, freq_high)].set_gain(self.gain)
            log('Starting {}-{}Hz injection to {} channel'.format(freq_low, freq_high, self.channel))
            inj['{} {}-{}Hz'.format(station, freq_low, freq_high)].start(ramptime=RAMP_TIME)  # START THE INJECTION
            self.t_start = gpsnow()
            # Wait for the duration of the injection so that injections do not overlap in time
            self.timer['Injection duration'] = DURATION
            self.timer['buffer time'] = 0
            self.clearflag = False
        
        @assert_IFO_inlock
        def run(self):
            if self.timer['Injection duration'] and not self.clearflag:
                # Stop the injection because its full duration has elapsed
                log('Injection finished')
                inj['{} {}-{}Hz'.format(station, freq_low, freq_high)].stop(ramptime=RAMP_TIME)  # STOP THE INJECTION
                self.t_end = gpsnow()
                inj['{} {}-{}Hz'.format(station, freq_low, freq_high)].clear()
                time.sleep(2)
                awg.awg_cleanup() # Erik says we need this b/c awg doesn't always close right
                # Pause for some more time before making the next injection
                log('Waiting for {} seconds before making the next injection...'.format(TIME_BETWEEN_INJECTIONS))
                self.timer['buffer time'] = TIME_BETWEEN_INJECTIONS 
                self.log_info = '{}, {:.0f}, {:.0f}, {}, {}, {}'.format(self.channel, self.t_start, self.t_end, freq_low, freq_high, gain)
                with open(LOG_FILE['file'], 'a') as f:
                    f.write(self.log_info + '\n')
                log(self.log_info)
                self.clearflag = True
            if self.timer['buffer time'] and self.clearflag:
                return True

    return INJECTION


########## Injection state generation ##############
itr = iter(params) # Make params dict an iterator
for i, exc in enumerate(params):
    # Make the state
    state = '{}_INJ'.format(exc)
    globals()[state] = gen_MAG_INJ(params[exc]['station'], params[exc]['location'], params[exc]['freq_low'], params[exc]['freq_high'], \
    params[exc]['gain'], params[exc]['index'])
    # Make the edges
    if i == 0:
        edges += [('TAKE_BACKGROUND', state)]
    if i > 0:
        prev_state = '{}_INJ'.format(next(itr))
        edges += [(prev_state, state)]
    if (i + 1) == len(params):
        edges += [(state, 'INJECTIONS_COMPLETE')]


# States
class INIT(GuardState):

    def main(self):
        return 'DOWN'


class DOWN(GuardState):
    index = 2
    goto = True
    
    def main(self):
        # Stop injections
        for name, injection in inj.items():
            log('Stopping ' + name)
            if injection.started:
                injection.stop(ramptime=RAMP_TIME)
        # Clear injections
        for name, injection in inj.items():
            log('Clearing ' + name)
            injection.clear()
            time.sleep(2)
            awg.awg_cleanup()
        # Turn off filter banks
        for station, channums in gds_paths.items():
            for channum in channums:
                ezca.get_LIGOFilter(CHAN.format(station, channum)).only_on('INPUT', 'OUTPUT', 'DECIMATION')
        # Turn off amps (H1 only)
        if site == 'H1':
            for unit in injparams.pwrunits:
                ezca['CDS-{}_OUTLETS_ALL_OFF_CMD'.format(unit)] = 1

        LOG_FILE['file'] = None
        manual['is_manual'] = False

        return True


class WAITING(GuardState):  # State to sit in most of the time
    index = 5

    def run(self):
        # Injections can run automatically if IFO is locked at specified day and time
        if date.today().weekday() == injparams.day and time.strftime('%H:%M:%S') == injparams.starttime and ezca['GRD-ISC_LOCK_STATE_N'] == injparams.nln_statenum:
            if site == 'H1':
                # Check for stand down alert
                if ezca['OPS-STANDDOWN_STATE'] != 0:
                    if ezca['CAL-INJ_EXTTRIG_ALERT_SOURCE'].lower() in standdown_sources:
                        notify('Stand down alert active! Not running injections')
                        return False
                    # Check if stand down is from KamLAND
                    elif ezca['OPS-STANDDOWN_EVENT_NAME'].lower() == 'superkam':
                        notify('KamLAND stand down alert active! Not running injections')
                        return False
                else:
                    return True
            else:
                return True
        # For running injections manually
        if ezca['GRD-PEM_MAG_INJ_TARGET'] == 'PREP_MANUAL_INJECTIONS':
            return True


class PREP_MANUAL_INJECTIONS(GuardState):
    index = 7
    
    def main(self):
        manual['is_manual'] = True
        # Find next GPS time that ends in 00
        #self.gps_req = ezca['FEC-24_TIME_DIAG']
        #self.inj_startgps = (int(math.ceil(self.gps_req / 100.0)) * 100)
        #log("Injections will start in {} seconds...".format(self.inj_startgps - self.gps_req))
        
    def run(self):
        #if self.inj_startgps == ezca['FEC-24_TIME_DIAG']:
        notify('Request INJECTIONS_COMPLETE to begin')
        return True

class TAKE_BACKGROUND(GuardState):
    index = 9
    request = False
    
    @assert_IFO_inlock
    def main(self):
        if LOG_FILE['file'] is None:
            LOG_FILE['file'] = os.path.join(LOG_DIR, '{:.0f}'.format(gpsnow()) + '.txt')
        self.t_start = gpsnow()
        self.timer['Background duration'] = DURATION
        self.timer['buffer time'] = 0
        self.clearflag = False

    @assert_IFO_inlock
    def run(self):
        if self.timer['Background duration'] and not self.clearflag:
            log('Background finished')
            self.t_end = gpsnow()
            # Pause for some time before making the first injection
            log('Waiting for {} seconds before starting injections...'.format(TIME_BETWEEN_INJECTIONS))
            self.timer['buffer time'] = TIME_BETWEEN_INJECTIONS 
            self.log_info = 'Background taken from {:.0f} to {:.0f}'.format(self.t_start, self.t_end)
            self.title = 'EXC CHANNEL, GPS START, GPS STOP, LOW FREQ, HIGH FREQ, GAIN'
            with open(LOG_FILE['file'], 'a') as f:
                f.write(self.log_info + '\n' + self.title + '\n')
            log(self.log_info)
            self.clearflag = True
        if self.timer['buffer time'] and self.clearflag:
            # Turn on amps (H1 only)
            if site == 'H1':
                for unit in injparams.pwrunits:
                    ezca['CDS-{}_OUTLET_1_ON_CMD'.format(unit)] = 1
                log('Waiting for amps to turn on...')
                time.sleep(10)
            return True


class INJECTIONS_COMPLETE(GuardState):
    index = 1000
    
    def main(self):
        notify('All injections complete! Returning to DOWN')
        self.timer['Inj complete'] = 10

    def run(self):
        if self.timer['Inj complete']:
            return 'DOWN'

